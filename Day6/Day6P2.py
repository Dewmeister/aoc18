from collections import Counter

coordinates = open('D6input.txt').readlines()
# print(coordinates)
xcoords = []
ycoords = []

for idx, coord in enumerate(coordinates):
    nextx, nexty = coord.split(', ')
    xcoords.append(int(nextx))
    ycoords.append(int(nexty))

bigx = max(xcoords)
bigy = max(ycoords)

size = 0
for idx1 in range(bigx):
    for idx2 in range(bigy):
        dtotal = 0
        for site in range(len(xcoords)):
            len2site = abs(xcoords[site] - idx1) + abs(ycoords[site]-idx2)
            dtotal += len2site
        if dtotal < 10000:
            size += 1

print('The total size of the safe area is:', str(size))
