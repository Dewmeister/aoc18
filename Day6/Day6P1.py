from collections import Counter

coordinates = open('D6input.txt').readlines()
# print(coordinates)
xcoords = []
ycoords = []

for idx, coord in enumerate(coordinates):
    nextx, nexty = coord.split(', ')
    xcoords.append(int(nextx))
    ycoords.append(int(nexty))

smallx = min(xcoords)
smally = min(xcoords)

xcoords = [item-smallx for item in xcoords]
ycoords = [item-smally for item in ycoords]

bigx = max(xcoords)
bigy = max(ycoords)

matrix = []
for idx1 in range(bigx):
    cols = []
    for idx2 in range(bigy):
        cols.append('.')
    matrix.append(cols)

for idx1 in range(bigx):
    for idx2 in range(bigy):
        lengths = []
        for site in range(len(xcoords)):
            len2site = abs(xcoords[site] - idx1) + abs(ycoords[site]-idx2)
            lengths.append(len2site)
        minval = min(lengths)
        place = lengths.index(minval)
        lengths.remove(minval)
        minval2 = min(lengths)
        if minval2 == minval:
            matrix[idx1][idx2] = '.'
        else:
            matrix[idx1][idx2] = place

infns = set()
for FLrow in matrix[0::bigx-1][:]:
    for row in FLrow:
        infns.add(row)
for FLcol in matrix[:][0::bigy-1]:
    for col in FLcol:
        infns.add(col)


for row in matrix:
    for idx, item in enumerate(row):
        if item in infns:
            row[idx] = '.'

score = Counter()
for row in matrix:
    for item in row:
        score[item] += 1


del score['.']
print(score.most_common(1))

