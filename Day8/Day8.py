data = open('D8test.txt').read()

dlist = [int(item) for item in data.split(' ')]


# def tree(it, op):
#     n, m = next(it), next(it)
#     print(n, m)
#     return op([tree(it, op) for _ in range(n)], [next(it) for _ in range(m)])

# print(tree(iter(dlist), lambda a, b: sum(a) + sum(b)))
#
# print(
#     tree(
#         iter(dlist),
#         lambda a, b: sum(a[i - 1] for i in b if 0 < i <= len(a)) if a else sum(b)))


def get_nodes(iterable):
    childcount, metalength = next(iterable), next(iterable)
    print('childcount: ', childcount)
    print('metalength: ', metalength)
    return [get_nodes(iterable) for _ in range(childcount)], [next(iterable) for _ in range(metalength)]


get_nodes(iter(dlist))



