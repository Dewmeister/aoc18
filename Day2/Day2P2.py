def cardify(string, index, wcchar):
    wc = string[0:index] + wcchar + string[index+1:]
    return wc


with open('D2P1input.txt') as puzzleinput:
    IDlist = [ID.strip() for ID in puzzleinput]
    approxIDs = set()
    for ID in IDlist:
        for idx in range(len(ID)):
            wcID = cardify(ID, idx, '*')
            if wcID in approxIDs:
                print('Match Found:', wcID.replace('*', ''), 'are the repeated letters')
                break
            else:
                approxIDs.add(wcID)
