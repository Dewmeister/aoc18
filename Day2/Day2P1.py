with open('D2P1input.txt') as puzzleinput:
    IDlist = [ID.strip() for ID in puzzleinput]
    cksum = 0
    ck2 = 0
    ck3 = 0
    for ID in IDlist:
        counts = {}
        have2 = 0
        have3 = 0
        for letter in ID:
            if letter in counts:
                counts[letter] += 1
            else:
                counts[letter] = 1
        for key in counts:
            if counts[key] == 2 and have2 == 0:
                ck2 += 1
                have2 = 1
            if counts[key] == 3 and have3 == 0:
                ck3 += 1
                have3 = 1
    cksum = ck2*ck3
    print('The final checksum is:', cksum)



