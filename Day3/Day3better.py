import re

matrix = []
sidelen = 1000
for i1 in range(sidelen):
    cols = []
    for i2 in range(sidelen):
        cols.append('*')
    matrix.append(cols)

# Problem 1
with open('D3P1input.txt') as puzzleinput:
    IDrx = re.compile('#[0-9]+')
    posrx = re.compile('@ [0-9,]+:')
    sizerx = re.compile('[0-9]+x[0-9]+')
    claimlist = [claim.strip() for claim in puzzleinput]
    overlap = 0
    for claim in claimlist:
        ID = IDrx.search(claim).group().strip('#')
        pos = [int(i) for i in posrx.search(claim).group().strip('@ :').split(',')]
        size = [int(i) for i in sizerx.search(claim).group().split('x')]
        for ix in range(size[0]):
            for iy in range(int(size[1])):
                cell = matrix[pos[0]+ix][pos[1]+iy]
                if cell is '*':
                    matrix[pos[0] + ix][pos[1] + iy] = '#'
                elif cell is '#':
                    matrix[pos[0] + ix][pos[1] + iy] = 'X'
                    overlap += 1

    print('The total overlapped area is:', overlap, " sq. in.")

for claim in claimlist:
    ID = IDrx.search(claim).group().strip('#')
    pos = [int(i) for i in posrx.search(claim).group().strip('@ :').split(',')]
    size = [int(i) for i in sizerx.search(claim).group().split('x')]
    ckunique = 0
    totalarea = size[0]*size[1]
    for ix in range(size[0]):
        for iy in range(int(size[1])):
            if matrix[pos[0]+ix][pos[1]+iy] is '#':
                ckunique += 1
    if ckunique == totalarea:
        print('The ID of the intact claim is:', ID)







