import re

with open('D3P1input.txt') as puzzleinput:
    posrx = re.compile('@ [0-9,]+:')
    sizerx = re.compile('[0-9]+x[0-9]+')
    claimlist = [claim.strip() for claim in puzzleinput]
    taken = set()
    marked = set()
    overlap = 0
    for claim in claimlist:
        pos = posrx.search(claim).group().strip('@ :')
        size = sizerx.search(claim).group()
        sizex, sizey = size.split('x')
        posx, posy = pos.split(',')
        for ix in range(int(sizex)):
            for iy in range(int(sizey)):
                coord = (str(int(posx)+ix), str(int(posy)+iy))
                if coord in taken and coord not in marked:
                    overlap += 1
                    marked.add(coord)
                else:
                    taken.add(coord)

print('The area of the overlapping claims is:', overlap, 'sq. in.')




