import re
from collections import Counter


def kwmaxv(dict):
    keys = list(dict.keys())
    vals = list(dict.values())
    return keys[vals.index(max(vals))]


with open('D4input.txt') as records:
    book = dict()
    daterx = re.compile('\d{2}-\d{2} ')
    timerx = re.compile('\d+:\d+')
    IDrx = re.compile('#\d+')
    actionrx = re.compile(' [Gfw]')
    dt = []
    act = []
    guards = {}
    for record in records:
        date = daterx.search(record).group().strip()
        time = timerx.search(record).group()
        action = actionrx.search(record).group().strip()
        if action == 'G':
            info = IDrx.search(record).group().strip('#')
        elif action == 'f':
            info = 0  # fallen asleep
        elif action == 'w':
            info = 1  # woken up
        dt.append(date + ' ' + time)
        act.append(info)

    book = sorted(zip(dt, act), key=lambda x: x[0])

    for page in book:
        date, time = page[0].split(' ')
        hr, minute = time.split(':')
        state = page[1]
        mntint = int(minute)
        if state not in (0, 1):
            ID = state
            if state not in guards.keys():
                guards[state] = ()
        elif state == 0:
            sleptat = mntint
        elif state == 1:
            guards[ID] += tuple(range(sleptat, mntint))
        else:
            print('IT DUN BORKED')
            quit(69)

    sumslp = {}
    mintslpt = {}
    mintfreq = {}
    for key, value in guards.items():
        if len(value) != 0:
            c = Counter(value)
            sumslp[key] = len(value)
            mintslpt[key] = c.most_common(1)[0][0]
            mintfreq[key] = c.most_common(1)[0][1]
            print(key, c.most_common(1))

    boinum = kwmaxv(sumslp)
    sleepiminute = mintslpt[boinum]
    print('The target guard is:', '#' + boinum, 'during minute:', sleepiminute, 'and the product is:', int(boinum)*sleepiminute)

    boinum2 = kwmaxv(mintfreq)
    sleepiminute2 = mintslpt[boinum2]
    print('The second target guard is:', '#' + boinum2, 'during minute:', sleepiminute2, 'and the product is:', int(boinum2)*sleepiminute2)


