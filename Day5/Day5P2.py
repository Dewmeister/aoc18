import string


def antipolar(letter1, letter2):
    case1 = 'undef'
    case2 = 'undef'
    if letter1.lower() == letter2.lower():
        if letter1 in string.ascii_lowercase:
            case1 = 0
        if letter2 in string.ascii_lowercase:
            case2 = 0
        if letter1 in string.ascii_uppercase:
            case1 = 1
        if letter2 in string.ascii_uppercase:
            case2 = 1
        if case1 == 'undef' or case2 == 'undef':
            return False
    if case1 != case2:
        return True
    else:
        return False


def polylen(polymer):
    changes = 1
    while changes == 1:
        changes = 0
        for index in range(len(polymer)):
            if index > 0:
                let1 = polymer[index - 1]
                let2 = polymer[index]
                aptf = antipolar(let1, let2)
                if aptf is True:
                    changes = 1
                    polymer[index - 1] = '*'
                    polymer[index] = '*'
        if changes == 1:
            polymer = [item for item in polymer if item != '*']
    return len(polymer)


pmr = open('D5input.txt').read()
low = string.ascii_lowercase
up = string.ascii_uppercase

lengths = []
for idx in range(len(low)):
    newpmr = [item for item in pmr if item is not low[idx] and item is not up[idx]]
    # print(newpmr)
    lengths.append(polylen(newpmr))

shortest = min(lengths)

# print(lengths)
print('After removing problem elements, the shortest length is:', shortest)
