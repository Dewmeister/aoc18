import string

# problems: when making a change, pos of letters after change changes so entries in idx2del become invalid
# must change in place but will invalidate a for loop
# rewrite using while?


def antipolar(letter1, letter2):
    case1 = 'undef'
    case2 = 'undef'
    if letter1.lower() == letter2.lower():
        if letter1 in string.ascii_lowercase:
            case1 = 0
        if letter2 in string.ascii_lowercase:
            case2 = 0
        if letter1 in string.ascii_uppercase:
            case1 = 1
        if letter2 in string.ascii_uppercase:
            case2 = 1
        if case1 == 'undef' or case2 == 'undef':
            return False
    if case1 != case2:
        return True
    else:
        return False


pmr = list(open('D5input.txt').read())
# print(pmr + '\n')
changes = 1
while changes == 1:
    changes = 0
    for idx in range(len(pmr)):
        if idx > 0:
            let1 = pmr[idx - 1]
            let2 = pmr[idx]
            aptf = antipolar(let1, let2)
            if aptf is True:
                changes = 1
                pmr[idx-1] = '*'
                pmr[idx] = '*'
                print('Change Needed at:', idx-1, 'and:', idx)
    if changes == 1:
            pmr = [item for item in pmr if item != '*']

print('The final polymer chain is:', pmr)
print(len(pmr))
