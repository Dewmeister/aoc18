import re
import string

instructions = open('D7input.txt').readlines()

letrx = re.compile(' [A-Z] ')
numwork = 5
workers = [0]*numwork
agenda = [-1]*numwork

timetable = dict()
for idx, letter in enumerate(string.ascii_uppercase):
    timetable[letter] = 61 + idx

pairs = []
steps = []
for line in instructions:
    letters = letrx.findall(line)
    entry = [item.strip() for item in letters]
    pairs.append(entry)
    for item in entry:
        if item not in steps:
            steps.append(item)

depends = {key: [] for key in steps}
for key, val in depends.items():
    for item in pairs:
        if item[1] == key and item[0] not in val:
            val.append(item[0])

done = 1
totime = -1
while done == 1:
    totime += 1
    for idx, item in enumerate(workers):
        if agenda[idx] > 0:
            agenda[idx] -= 1
        if agenda[idx] == 0:
            workers[idx] = 0
            for val in depends.values():
                if item in val:
                    val.remove(item)

    options = sorted([k for k, v in depends.items() if len(v) == 0])

    for idx, item in enumerate(workers):
        if len(options) > 0 and item == 0:
                choice = options.pop(0)
                del depends[choice]
                workers[idx] = choice
                agenda[idx] = timetable[choice]
    # print('o', options)
    # print('dep', depends.items())
    # print('w', workers)
    # print('a', agenda)
    # print()
    if workers == [0]*numwork:
        done = 0

print('The total time required is:', totime)
