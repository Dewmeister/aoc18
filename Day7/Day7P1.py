import re

instructions = open('D7input.txt').readlines()

letrx = re.compile(' [A-Z] ')

pairs = []
for line in instructions:
    letters = letrx.findall(line)
    entry = [item.strip() for item in letters]
    pairs.append(entry)

done = 1
order = []
while done == 1:
    prereqs = [item[0] for item in pairs]
    actions = [item[1] for item in pairs]
    print('*', pairs)
    print('p', prereqs)
    print('a', actions)
    options = [item for item in prereqs if item not in actions]
    choice = min(options)
    if choice not in order:
        order.append(choice)
    if len(actions) == 1:
        order.append(actions[0])
    for idx, item in enumerate(pairs):
        if item[0] == choice:
            del pairs[idx]
    if len(pairs) == 0:
        done = 0


print(''.join(order))
# print(prereqs, actions)
# print(options)
# print(choice)

