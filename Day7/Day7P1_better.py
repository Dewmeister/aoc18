import re

instructions = open('D7input.txt').readlines()

letrx = re.compile(' [A-Z] ')

pairs = []
steps = []
for line in instructions:
    letters = letrx.findall(line)
    entry = [item.strip() for item in letters]
    pairs.append(entry)
    for item in entry:
        if item not in steps:
            steps.append(item)

print('Pairs:', pairs)

depends = {key: [] for key in steps}
for key, val in depends.items():
    for item in pairs:
        if item[1] == key and item[0] not in val:
            val.append(item[0])

print("Dependencies:", depends.items())

done = 0
order = []
while done == 0:
    options = [k for k, v in depends.items() if len(v) == 0]
    choice = min(options)
    order.append(choice)
    del depends[choice]
    for val in depends.values():
        if choice in val:
            val.remove(choice)
    if len(depends) == 0:
        done = 1

print(''.join(order))
