freqlist = open('D1P1input.txt').read().split('\n')
# same as before. read() is necessary because open() returns a file object
freqhist = set()  # initialize a set (faster to search through than list)
cf = 0
while True:
    for f in freqlist:
        cf += int(f)
        if cf in freqhist:
            print('The first repeated frequency is:', cf)
            quit()
        freqhist.add(cf)

