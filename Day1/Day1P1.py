import re
# wanted to do this using regex to sort freq changes in to positive and negative
# lists for swag points
freqlist = open('D1P1input.txt').read().split('\n')  # open the text file, read it, split on newline char
print('All freqs are:', freqlist)
# initialize pos and neg lists
posfreqs = []
negfreqs = []
for freq in freqlist:
    # regex to grab and sort positive, negative freqs
    pmatch = re.search('\+[0-9]+', freq)
    nmatch = re.search('-[0-9]+', freq)
    if pmatch is not None:
        posfreqs.append(int(pmatch.group()))
    elif nmatch is not None:
        negfreqs.append(int(nmatch.group()))

print('Pos freqs are:', posfreqs)
print('Neg freqs are:', negfreqs)
finalans = sum(posfreqs) + sum(negfreqs)

print('The resulting frequency is:', finalans)
